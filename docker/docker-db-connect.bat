@echo off

echo ######################################
echo                 START
echo ######################################

REM We need to copy across some database properties to 
REM successfully connect and access the database locally
REM on MySQLWorkbench

echo [+] Showing running containers...
docker ps -a

echo [+] Copying across dbConnector.properties to 1st Tomcat...
docker cp dbConnector.properties t9s-dbda-91:/usr/local/tomcat/webapps/dbanalyzer/WEB-INF/classes
echo [+] Restarting 1st Tomcat...
docker restart t9s-dbda-91

echo [+] Copying across dbConnector.properties to 2nd Tomcat...
docker cp dbConnector.properties t9s-dbda-92:/usr/local/tomcat/webapps/dbanalyzer/WEB-INF/classes
echo [+] Restarting 2nd Tomcat...
docker restart t9s-dbda-92

echo [+] Completed successfully. Attempt connection via MySQLWorkbench now

echo ######################################
echo                 DONE
echo ######################################