@echo off

echo ######################################
echo                 START
echo ######################################

REM Importing MySQL Server image

echo [+] Copying MySQL server image...
docker load --input C:\vm_share\mysql\mysql-server.tar
echo [+] Copying successful
FOR /F "tokens=*" %%i IN ('docker images -f "dangling=true" -q') do (SET imageId=%%i)

echo [+] Tagging MySQL image
docker tag %imageId% mysql-server:latest
echo [+] Tagging successful


REM Importing Apache Tomcat Server image

echo [+] Copying Tomcat server image...
docker load --input C:\vm_share\tomcat-webapps\tomcat9-server.tar
echo [+] Copying successful

REM Importing HAProxy image - which wasn't provided to us
REM If a HAProxy image is provided we can simply import it
REM much like we've done before with "docker import"

echo [+] Pulling HAProxy from DockerHub...
docker pull haproxy
echo [+] Pull successful. HAProxy image acquired

echo [+] Starting up containers with Docker-Compose
docker-compose -f docker-compose.yml up

REM To stop the containers, run the above command replacing 'up' with 'down'

echo ######################################
echo                 DONE
echo ######################################
