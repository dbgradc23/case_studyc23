@echo off

REM After terminating the currently running batch job
REM run this script to shutdown all images and servers
REM while cleaning up successfully

echo ######################################
echo                 START
echo ######################################

echo [+] Cleaning up containers...
docker-compose -f docker-compose.yml down

echo [+] Checking servers...
docker ps -a
echo [+] Container clean-up successful

echo [+] Removing images...
docker rmi haproxy mysql-server tomcat9-server

echo [+] Checking images...
docker images
echo [+] Image clean-up successful

echo[+] Providing overview...
docker info

echo ######################################
echo                 DONE
echo ######################################