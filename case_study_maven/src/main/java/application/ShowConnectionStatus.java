package application;

public class ShowConnectionStatus {

	public String returnStatus(boolean status) {
		if(status == true) {
			return "Connection Successful";
		}
		else {
			return "No Connection to the Database";
		}
	}
}
